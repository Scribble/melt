---1 T4_Chest---
Chest(1939) - class: chestwarrioraluhigh
Chest(1942) - class: chestwarriorghahigh
Chest(1945) - class: chestwarriorhigh
Chest(1948) - class: chestwarriorshohigh
Chest(3985) - class: chestwarrioraluhighlocked
Chest(3988) - class: chestwarriorghahighlocked
Chest(3991) - class: chestwarriorhighlocked
Chest(3994) - class: chestwarriorshohighlocked
Sarcophagus(4859) - class: coffinwarrioraluhigh
Sarcophagus(4860) - class: coffinwarrioraluhighlocked
Sarcophagus(4865) - class: coffinwarriorghahigh
Sarcophagus(4866) - class: coffinwarriorghahighlocked
Sarcophagus(4871) - class: coffinwarriorhigh
Sarcophagus(4872) - class: coffinwarriorhighlocked
Sarcophagus(4877) - class: coffinwarriorshohigh
Sarcophagus(4878) - class: coffinwarriorshohighlocked
Lady Jaera's Tomb(6042) - class: mageacademycoffinwarriorhighlocked
Chest(7494) - class: chestclothinghighlocked
Chest(7495) - class: chestclothinghigh
Sarcophagus(7496) - class: coffinclothinghigh
Sarcophagus(7497) - class: coffinclothinghighlocked
Small Chest(7887) - class: chestsoulfearingvestrylow



---2 Tx_AdminChest---
Chest(5919) - class: chestadminhigh



---3 T3_Chest---
Chest(1941) - class: chestwarrioralumed
Chest(1944) - class: chestwarriorghamed
Chest(1947) - class: chestwarriormed
Chest(1950) - class: chestwarriorshomed
Chest(3987) - class: chestwarrioralumedlocked
Chest(3990) - class: chestwarriorghamedlocked
Chest(3993) - class: chestwarriormedlocked
Chest(3996) - class: chestwarriorshomedlocked
Sarcophagus(4863) - class: coffinwarrioralumed
Sarcophagus(4864) - class: coffinwarrioralumedlocked
Sarcophagus(4869) - class: coffinwarriorghamed
Sarcophagus(4870) - class: coffinwarriorghamedlocked
Sarcophagus(4875) - class: coffinwarriormed
Sarcophagus(4876) - class: coffinwarriormedlocked
Sarcophagus(4881) - class: coffinwarriorshomed
Sarcophagus(4882) - class: coffinwarriorshomedlocked
Chest(5918) - class: chestadminmedium
Chest(7500) - class: chestclothingmed
Small Chest(7888) - class: chestsoulfearingvestrymed



---4 T2_Chest---
Chest(1317) - class: chestsewertreasure2
Chest(1940) - class: chestwarrioralulow
Chest(1943) - class: chestwarriorghalow
Chest(1946) - class: chestwarriorlow
Chest(1949) - class: chestwarriorsholow
Chest(3986) - class: chestwarrioralulowlocked
Chest(3989) - class: chestwarriorghalowlocked
Chest(3992) - class: chestwarriorlowlocked
Chest(3995) - class: chestwarriorsholowlocked
Sarcophagus(4861) - class: coffinwarrioralulow
Sarcophagus(4862) - class: coffinwarrioralulowlocked
Sarcophagus(4867) - class: coffinwarriorghalow
Sarcophagus(4868) - class: coffinwarriorghalowlocked
Sarcophagus(4873) - class: coffinwarriorlow
Sarcophagus(4874) - class: coffinwarriorlowlocked
Sarcophagus(4879) - class: coffinwarriorsholow
Sarcophagus(4880) - class: coffinwarriorsholowlocked
Chest(7493) - class: chestclothinglow
Sarcophagus(7498) - class: coffinclothinglow
Sarcophagus(7499) - class: coffinclothingmed



---6 T1_NoobChest---
Chest(27242) - class: chestclothingnewbie



---13 T4_Money---
Chest(1917) - class: chestglitterhigh
Chest(1926) - class: chestminerhigh
Chest(1929) - class: chestmoneyhigh
Chest(1933) - class: chestthiefhigh
Chest(3963) - class: chestglitterhighlocked
Chest(3972) - class: chestminerhighlocked
Chest(3975) - class: chestmoneyhighlocked
Chest(3979) - class: chestthiefhighlocked
Sarcophagus(4815) - class: coffinglitterhigh
Sarcophagus(4816) - class: coffinglitterhighlocked
Sarcophagus(4833) - class: coffinminerhigh
Sarcophagus(4834) - class: coffinminerhighlocked
Sarcophagus(4839) - class: coffinmoneyhigh
Sarcophagus(4840) - class: coffinmoneyhighlocked
Sarcophagus(4847) - class: coffinthiefhigh
Sarcophagus(4848) - class: coffinthiefhighlocked



---15 T3_Money---
Chest(1238) - class: chestglendenpicks
Chest(1919) - class: chestglittermed
Chest(1928) - class: chestminermed
Chest(1931) - class: chestmoneymed
Chest(1935) - class: chestthiefmed
Chest(3965) - class: chestglittermedlocked
Chest(3974) - class: chestminermedlocked
Chest(3977) - class: chestmoneymedlocked
Chest(3981) - class: chestthiefmedlocked
Sarcophagus(4819) - class: coffinglittermed
Sarcophagus(4820) - class: coffinglittermedlocked
Sarcophagus(4837) - class: coffinminermed
Sarcophagus(4838) - class: coffinminermedlocked
Sarcophagus(4843) - class: coffinmoneymed
Sarcophagus(4844) - class: coffinmoneymedlocked
Sarcophagus(4851) - class: coffinthiefmed
Sarcophagus(4852) - class: coffinthiefmedlocked



---16 T2_Money---
Chest(1238) - class: chestglendenpicks
Chest(1239) - class: chestglendentreasure
Chest(1239) - class: chestglendentreasure
Chest(1316) - class: chestsewertreasure
Chest(1317) - class: chestsewertreasure2
Chest(1317) - class: chestsewertreasure2
Chest(1918) - class: chestglitterlow
Chest(1927) - class: chestminerlow
Chest(1930) - class: chestmoneylow
Chest(1934) - class: chestthieflow
Chest(1953) - class: chestthievesdenaward
Chest(3964) - class: chestglitterlowlocked
Chest(3973) - class: chestminerlowlocked
Chest(3976) - class: chestmoneylowlocked
Chest(3980) - class: chestthieflowlocked
Sarcophagus(4817) - class: coffinglitterlow
Sarcophagus(4818) - class: coffinglitterlowlocked
Sarcophagus(4835) - class: coffinminerlow
Sarcophagus(4836) - class: coffinminerlowlocked
Sarcophagus(4841) - class: coffinmoneylow
Sarcophagus(4842) - class: coffinmoneylowlocked
Sarcophagus(4849) - class: coffinthieflow
Sarcophagus(4850) - class: coffinthieflowlocked
Strongbox(5162) - class: chestlubziklancider



---18 T1_Money---
Chest(1253) - class: chestgreenmiregems
Chest(27244) - class: chestglitternewbie



---32 T6_BossesGeneral---
Tahuirea's Cache(27278) - class: chesttiulerea
Yaruldi's Hoard(27281) - class: chestorphanageyaruldi
Black Marrow Reliquary(30792) - class: chestblackmarrowreliquarycaulcano
Black Marrow Reliquary(30793) - class: chestblackmarrowreliquaryfloatingbridge
Black Marrow Reliquary(30794) - class: chestblackmarrowreliquaryfloatingtower
Black Marrow Reliquary(30795) - class: chestblackmarrowreliquaryoasis
Black Marrow Reliquary(30796) - class: chestblackmarrowreliquary
Black Marrow Reliquary(30797) - class: chestblackmarrowreliquaryburningtower



---59 T5_ChestBoss---
Chest(5902) - class: chestfrore
Platinum Legion Quartermaster's Chest(29385) - class: chestquartermasterplatinum
Silver Legion Quartermaster's Chest(29386) - class: chestquartermastersilver
Copper Legion Quartermaster's Chest(29387) - class: chestquartermastercopper
Gold Legion Quartermaster's Chest(29388) - class: chestquartermastergold



---92 T1_ChestGreenmire1---
Chest(1251) - class: chestgreenmirearmor



---93 T1_ChestGreenmire2---
Chest(1252) - class: chestgreenmirecrown



---313 T2_ChestColierMine---
Treasure Chest(1540) - class: chestcolierminegold



---317 T5_Chest_Aerlinthe---
Lady of Aerlinthe's Chest(7408) - class: chestaerfalle
Lady of Aerlinthe's Ornate Chest(28047) - class: chestaerfalleuber
Chest of Confiscations(29470) - class: chestoswaldprison



---334 T5_ChestBossExtraLoot---
Treasury of the Order(8515) - class: chestherald
Sanctum of the Hopeslayer Chest(8784) - class: chestlairhopeslayer



---338 T4_SteelChestScrolls---
Steel Chest(8999) - class: chestvirindicamplootlocked



---339 T5_DirectiveCache---
Directive's Cache(9286) - class: chestvirindidirective



---340 T4_MasterHolding---
Master's Holding(9287) - class: chestvirindimaster



---341 T5_SingularityTrove---
Singularity Trove(9288) - class: chestvirindisingularity
Martinate Singularity Trove(14871) - class: chestmartinatetrove
Sanatorium Chest(22907) - class: chestaerbax2
Noble Brace Chest(28431) - class: chestmorgluukatlatl
Noble War Maul Chest(28432) - class: chestmorgluukaxe
Noble Longbow Chest(28433) - class: chestmorgluukbow
Noble CrossbowChest(28434) - class: chestmorgluukcrossbow
Noble Stiletto Chest(28435) - class: chestmorgluukdirk
Noble Treasure Chest(28436) - class: chestmorgluukgeneric
Noble Katar Chest(28437) - class: chestmorgluukkatar
Noble Morning Star Chest(28438) - class: chestmorgluukmace
Noble Scepter Chest(28439) - class: chestmorgluukscepter
Noble Swordstaff Chest(28440) - class: chestmorgluukspear
Noble Quarterstaff Chest(28441) - class: chestmorgluukstaff
Noble Rapier Chest(28442) - class: chestmorgluuksword



---349 Tx_AluvianCasinoChest---
Monty's Golden Chest(9460) - class: chestgambleralu



---350 Tx_GharundimCasinoChest---
Arshid's Golden Chest(9461) - class: chestgamblergha



---351 Tx_ShoCasinoChest---
Gan-Zo's Golden Chest(9462) - class: chestgamblersho



---365 T3_HeadRaiderCache---
Hea Raiders' Cache(10934) - class: chesthearaider_xp



---395 395---
Chest(12163) - class: chesttombloot



---410 T2_RunedChest---
Runed Chest(22568) - class: chestquestlockedlow
Runed Chest(22572) - class: chestquestunlockedlow
Runed Chest(22802) - class: chestquestlockedmiddaiklos
Runed Chest(22807) - class: chestquestunlockedlowhedgedplatform
Runed Chest(23601) - class: chestquestlockedlowpoia
Runed Chest(23602) - class: chestquestlockedlowpoib
Runed Chest(23607) - class: chestquestunlockedlowpoia
Runed Chest(23608) - class: chestquestunlockedlowpoib
Runed Chest(24668) - class: chestquestlockedlowpoic
Runed Chest(24669) - class: chestquestlockedlowpoid
Runed Chest(24670) - class: chestquestlockedlowpoie
Runed Chest(24677) - class: chestquestunlockedlowpoic
Runed Chest(24678) - class: chestquestunlockedlowpoid
Runed Chest(24679) - class: chestquestunlockedlowpoie
Runed Chest(26606) - class: chestquestlockedlowpoif
Runed Chest(26607) - class: chestquestlockedlowpoig
Runed Chest(26619) - class: chestquestunlockedlowpoif
Runed Chest(26620) - class: chestquestunlockedlowpoig
Runed Chest(27379) - class: chestquestdrudgefight



---411 T3_RunedChest---
Runed Chest(22570) - class: chestquestlockedmid
Runed Chest(22576) - class: chestquestunlockedmid
Runed Chest(22808) - class: chestquestlockedmidbanderlingbandittower
Runed Chest(23603) - class: chestquestlockedmidpoia
Runed Chest(23604) - class: chestquestlockedmidpoib
Runed Chest(23609) - class: chestquestunlockedmidpoia
Runed Chest(23610) - class: chestquestunlockedmidpoib
Runed Chest(24671) - class: chestquestlockedmidpoic
Runed Chest(24672) - class: chestquestlockedmidpoid
Runed Chest(24673) - class: chestquestlockedmidpoie
Runed Chest(24680) - class: chestquestunlockedmidpoic
Runed Chest(24681) - class: chestquestunlockedmidpoid
Runed Chest(24682) - class: chestquestunlockedmidpoie
Runed Chest(26608) - class: chestquestlockedmidpoif
Runed Chest(26609) - class: chestquestlockedmidpoig
Runed Chest(26621) - class: chestquestunlockedmidpoif
Runed Chest(26622) - class: chestquestunlockedmidpoig
Runed Chest(27375) - class: chestquestcoinattachment
Runed Chest(27382) - class: chestquestrelic



---412 T4_RunedChest---
Runed Chest(22567) - class: chestquestlockedhigh
Runed Chest(22571) - class: chestquestunlockedhigh
Runed Chest(23599) - class: chestquestlockedhighpoia
Runed Chest(23600) - class: chestquestlockedhighpoib
Runed Chest(23605) - class: chestquestunlockedhighpoia
Runed Chest(23606) - class: chestquestunlockedhighpoib
Runed Chest(24665) - class: chestquestlockedhighpoic
Runed Chest(24666) - class: chestquestlockedhighpoid
Runed Chest(24667) - class: chestquestlockedhighpoie
Runed Chest(24674) - class: chestquestunlockedhighpoic
Runed Chest(24675) - class: chestquestunlockedhighpoid
Runed Chest(24676) - class: chestquestunlockedhighpoie
Runed Chest(26604) - class: chestquestlockedhighpoif
Runed Chest(26605) - class: chestquestlockedhighpoig
Runed Chest(26617) - class: chestquestunlockedhighpoif
Runed Chest(26618) - class: chestquestunlockedhighpoig
Runed Chest(27374) - class: chestquestaubereanmap
Runed Chest(27380) - class: chestquestlairhomunculus
Runed Chest(27383) - class: chestquestrestingplace
Runed Chest(27384) - class: chestquestwizardsblade



---413 T5_RunedChest---
Runed Chest(22566) - class: chestquestlockedextreme
Runed Chest(23597) - class: chestquestlockedextremepoia
Runed Chest(23598) - class: chestquestlockedextremepoib
Runed Chest(24662) - class: chestquestlockedextremepoic
Runed Chest(24663) - class: chestquestlockedextremepoid
Runed Chest(24664) - class: chestquestlockedextremepoie
Runed Chest(26602) - class: chestquestlockedextremepoif
Runed Chest(26603) - class: chestquestlockedextremepoig
Runed Chest(27376) - class: chestquestdarkmonolith
Runed Chest(27377) - class: chestquestdarktowers
Runed Chest(27381) - class: chestquestgibbering
Runed Chest(30392) - class: chestquestlittlestniffis



---414 T1_RunedChest---
Runed Chest(22569) - class: chestquestlockedlowratnest
Runed Chest(22573) - class: chestquestunlockedlowdrudgehideout
Runed Chest(22574) - class: chestquestunlockedlowdungeonfern
Runed Chest(22575) - class: chestquestunlockedlowratnest
Sarcophagus(22577) - class: coffinquestlockedlowholtburgdungeon
Runed Chest(22800) - class: chestquestlockedlowgolemmound
Runed Chest(22801) - class: chestquestlockedlowlichhut
Runed Chest(22803) - class: chestquestlockedmidhilltopdrudgetower
Runed Chest(22804) - class: chestquestlockedmidholtburgruin
Runed Chest(22805) - class: chestquestlockedmidmushroomaltar
Runed Chest(22806) - class: chestquestunlockedlowdaiklos
Runed Chest(26610) - class: chestquestlockednewbiepoia
Runed Chest(26611) - class: chestquestlockednewbiepoib
Runed Chest(26612) - class: chestquestlockednewbiepoic
Runed Chest(26613) - class: chestquestlockednewbiepoid
Runed Chest(26614) - class: chestquestlockednewbiepoie
Runed Chest(26615) - class: chestquestlockednewbiepoif
Runed Chest(26616) - class: chestquestlockednewbiepoig
Runed Chest(26623) - class: chestquestunlockednewbiepoia
Runed Chest(26624) - class: chestquestunlockednewbiepoib
Runed Chest(26625) - class: chestquestunlockednewbiepoic
Runed Chest(26626) - class: chestquestunlockednewbiepoid
Runed Chest(26627) - class: chestquestunlockednewbiepoie
Runed Chest(26628) - class: chestquestunlockednewbiepoif
Runed Chest(26629) - class: chestquestunlockednewbiepoig



---421 T5_OverlordChest---
The Overlord's Chest(8972) - class: chestoverlord
Reinforced Mahogany Chest(23085) - class: chestvalleydeathhigh
Forbidden Chest(27301) - class: chestforbidden
Runed Chest(27378) - class: chestquestdeepcaverns



---422 T4_ReinforcedOakenChest---
Reinforced Oaken Chest(23086) - class: chestvalleydeathlow



---449 T6_General---
Sturdy Steel Chest(24476) - class: chestgeneralextremelocked
Sturdy Steel Chest(24476) - class: chestgeneralextremelocked
Sturdy Steel Chest(24476) - class: chestgeneralextremelocked



---454 T4_General---
Chest(1215) - class: chestthievesden
Chest(1237) - class: chestglendencheese
Chest(1912) - class: chestfoodhigh
Chest(1914) - class: chestgeneralhigh
Chest(1920) - class: chesthealerhigh
Chest(1936) - class: chestutilityhigh
Chest(3958) - class: chestfoodhighlocked
Chest(3960) - class: chestgeneralhighlocked
Chest(3966) - class: chesthealerhighlocked
Chest(3982) - class: chestutilityhighlocked
Sarcophagus(4805) - class: coffinfoodhigh
Sarcophagus(4806) - class: coffinfoodhighlocked
Sarcophagus(4809) - class: coffingeneralhigh
Sarcophagus(4810) - class: coffingeneralhighlocked
Sarcophagus(4821) - class: coffinhealerhigh
Sarcophagus(4822) - class: coffinhealerhighlocked
Sarcophagus(4853) - class: coffinutilityhigh
Sarcophagus(4854) - class: coffinutilityhighlocked



---456 T3_General--- convert these to T2_General
Chest(1916) - class: chestgeneralmed
Chest(1922) - class: chesthealermed
Chest(1938) - class: chestutilitymed
Chest(3962) - class: chestgeneralmedlocked
Chest(3968) - class: chesthealermedlocked
Chest(3984) - class: chestutilitymedlocked
Sarcophagus(4813) - class: coffingeneralmed
Sarcophagus(4814) - class: coffingeneralmedlocked
Sarcophagus(4825) - class: coffinhealermed
Sarcophagus(4826) - class: coffinhealermedlocked
Sarcophagus(4857) - class: coffinutilitymed
Sarcophagus(4858) - class: coffinutilitymedlocked



---457 T2_General--- convert these to T1_Warrior
Chest(1237) - class: chestglendencheese
Chest(1317) - class: chestsewertreasure2
Chest(1913) - class: chestfoodlow
Chest(1915) - class: chestgenerallow
Chest(1921) - class: chesthealerlow
Chest(1937) - class: chestutilitylow
Chest(3959) - class: chestfoodlowlocked
Chest(3961) - class: chestgenerallowlocked
Chest(3967) - class: chesthealerlowlocked
Chest(3983) - class: chestutilitylowlocked
Sarcophagus(4807) - class: coffinfoodlow
Sarcophagus(4808) - class: coffinfoodlowlocked
Sarcophagus(4811) - class: coffingenerallow
Sarcophagus(4812) - class: coffingenerallowlocked
Sarcophagus(4823) - class: coffinhealerlow
Sarcophagus(4824) - class: coffinhealerlowlocked
Sarcophagus(4855) - class: coffinutilitylow
Sarcophagus(4856) - class: coffinutilitylowlocked
Chest(5155) - class: chestdah
Strongbox(5204) - class: chestciderutilitylow
Armory Chest(8210) - class: chestxara



---459 T1_Warrior---
Chest(1316) - class: chestsewertreasure
Chest(1932) - class: chestpoor
Chest(3978) - class: chestpoorlocked
Sarcophagus(4845) - class: coffinpoor
Sarcophagus(4846) - class: coffinpoorlocked
Chest(27243) - class: chestgeneralnewbie



---460 T4_Magic---
Chest(1923) - class: chestmagichigh
Chest(2544) - class: chestscholarhigh
Chest(3969) - class: chestmagichighlocked
Sarcophagus(4827) - class: coffinmagichigh
Sarcophagus(4828) - class: coffinmagichighlocked
Laboratory Supply Chest(6776) - class: sylsfearchestmagichighlocked
Chest(7297) - class: chestscholarhighlocked
Small Chest(7811) - class: chestsoulfearingvestry
Mistress' Chest(8514) - class: chestadja
Chest(9168) - class: chestmartinelocked



---462 T3_Magic---
Chest(1925) - class: chestmagicmed
Chest(3971) - class: chestmagicmedlocked
Sarcophagus(4831) - class: coffinmagicmed
Sarcophagus(4832) - class: coffinmagicmedlocked
Chest(7784) - class: chestmagicmedmastery
Sarcophagus(7809) - class: coffinlichgen
Small Chest(7889) - class: chestsoulfearingvestryhigh
Ancient Tomb(8374) - class: coffinxara



---463 T2_Magic---
Chest(1316) - class: chestsewertreasure
Chest(1924) - class: chestmagiclow
Chest(3970) - class: chestmagiclowlocked
Sarcophagus(4829) - class: coffinmagiclow
Sarcophagus(4830) - class: coffinmagiclowlocked
Chest(7785) - class: chestmagiclowineptitude
Sarcophagus(7808) - class: coffinzombiegen



---465 T1_Magic---
Chest(27245) - class: chestmagicnewbie



